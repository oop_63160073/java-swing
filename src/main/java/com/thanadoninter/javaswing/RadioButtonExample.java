/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/**
 *
 * @author Acer
 */
public class RadioButtonExample extends JFrame {

    public RadioButtonExample() {
        JRadioButton r1 = new JRadioButton("A) Male");
        JRadioButton r2 = new JRadioButton("B) Female");
        r1.setBounds(75, 50, 100, 30);
        r2.setBounds(75, 100, 100, 30);
        ButtonGroup bg = new ButtonGroup();
        bg.add(r1);
        bg.add(r2);
        JButton b = new JButton("click");
        b.setBounds(75,150,80,30);
        b.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(r1.isSelected()){
                    JOptionPane.showMessageDialog(RadioButtonExample.this, "You are male.");
                }
                
                if(r2.isSelected()){
                    JOptionPane.showMessageDialog(RadioButtonExample.this, "You are female.");
                }
            }
            
        });
        add(r1);
        add(r2);
        add(b);
        setSize(300, 300);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public static void main(String[] args) {
        new RadioButtonExample();
    }
}
