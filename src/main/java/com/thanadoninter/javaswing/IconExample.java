/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Acer
 */
public class IconExample {

    public IconExample() {
        JFrame f = new JFrame();
        Image icon = Toolkit.getDefaultToolkit().getImage("C:\\Users\\Acer\\Desktop\\raiden icons.png");
        f.setIconImage(icon);
        f.setLayout(null);
        f.setSize(400, 400);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        new IconExample();
    }
}
