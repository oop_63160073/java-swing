/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import java.awt.FlowLayout;  
import javax.swing.JButton;  
import javax.swing.JFrame;  
import javax.swing.JLabel;  
import javax.swing.JPanel; 
/**
 *
 * @author Acer
 */
public class JFrameExample {
    public static void main(String[] args) {
        JFrame f = new JFrame("JFrame Example");
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        JLabel l = new JLabel("JFrame by example");
        JButton b = new JButton();
        b.setText("Button");
        panel.add(l);panel.add(b);
        f.add(panel);
        f.setSize(200,300);
        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
        
    }
}
