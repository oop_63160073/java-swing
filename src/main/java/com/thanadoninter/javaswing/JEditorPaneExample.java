/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author Acer
 */
public class JEditorPaneExample {

    JFrame myFrame = null;

    public static void main(String[] args) {
        (new JEditorPaneExample()).test();
    }

    private void test() {
        myFrame = new JFrame("JEditorPane Test");
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myFrame.setSize(400, 200);
        JEditorPane ep = new JEditorPane();
        ep.setContentType("text/html");
        ep.setText("<h1>Sleeping</h1><p>Sleeping is necessary for a healthy body."
                        +" But sleeping in unnecessary times may spoil our health, wealth and studies."
                        +" Doctors advise that the sleeping at improper timings may lead for obesity during the students days.</p>");
        myFrame.setContentPane(ep);
        myFrame.setVisible(true);

    }
}
