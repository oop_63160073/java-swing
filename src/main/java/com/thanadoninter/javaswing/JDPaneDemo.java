/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

/**
 *
 * @author Acer
 */
public class JDPaneDemo extends JFrame {

    public JDPaneDemo() {
        CustomDesktopPane desktopPane = new CustomDesktopPane();
        Container contentpane = getContentPane();
        contentpane.add(desktopPane, BorderLayout.CENTER);
        desktopPane.display(desktopPane);

        setTitle("JDesktopPane Example");
        setSize(300, 500);
        setVisible(true);
    }

    public static void main(String[] args) {
        new JDPaneDemo();
    }

    class CustomDesktopPane extends JDesktopPane {

        int numFrames = 3, x = 30, y = 30;

        public void display(CustomDesktopPane dp) {
            for (int i = 0; i < numFrames; ++i) {
                JInternalFrame jFrame = new JInternalFrame("Internal Frame" + i, true, true, true, true);

                jFrame.setBounds(x, y, 250, 85);
                Container c1 = jFrame.getContentPane();
                c1.add(new JLabel("I love my country"));
                dp.add(jFrame);
                jFrame.setVisible(true);
                y += 85;
            }
        }
    }

}
