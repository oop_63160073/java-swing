/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Acer
 */
public class ComboBoxExample extends JFrame {

    public ComboBoxExample() {
        String pl[]={"C","C++","C#","Java","PHP","SQL"}; 
        JComboBox cb=new JComboBox(pl);
        cb.setBounds(50,50,90,20);
        JButton b = new JButton("Select");
        b.setBounds(50,100,90,20);
        JLabel l = new JLabel("");
        l.setBounds(50,30,400,20);
        add(cb);add(b);add(l);
        setLayout(null);
        setSize(400,500);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Programming Language Selected: " +( 
                        (String) cb.getItemAt(cb.getSelectedIndex()));
                l.setText(data);
            }
            
        });
    }
    
    public static void main(String[] args) {
        new ComboBoxExample();
    }
}
