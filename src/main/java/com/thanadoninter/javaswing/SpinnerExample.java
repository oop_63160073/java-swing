/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Acer
 */
public class SpinnerExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Spinner Example");
        SpinnerModel value = new SpinnerNumberModel(5, 0, 10, 1);
        JSpinner spinner = new JSpinner(value);
        spinner.setBounds(100, 100, 50, 30);
        JLabel l = new JLabel();
        l.setHorizontalAlignment(JLabel.CENTER);
        l.setSize(250, 100);
        
        f.add(spinner);
        f.add(l);
        f.setSize(300, 300);
        f.setLayout(null);
        f.setVisible(true);
        
        spinner.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e) {
                l.setText("Value : "+ ((JSpinner)e.getSource()).getValue());
            }
            
        });
    }

}
