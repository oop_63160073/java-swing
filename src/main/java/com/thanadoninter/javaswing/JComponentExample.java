/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import java.awt.Color;  
import java.awt.Graphics;  
import javax.swing.JComponent;  
import javax.swing.JFrame;
/**
 *
 * @author Acer
 */

class MyJComponent extends JComponent {  
      public void paint(Graphics g) {  
        g.setColor(Color.green);  
        g.fillRect(30, 30, 100, 100);  
      }  
}

public class JComponentExample {
    public static void main(String[] args) {
        MyJComponent com = new MyJComponent();
        
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame f = new JFrame("JComponent Example");
        f.setSize(300,200);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(com);
        f.setVisible(true);
    }
}
