/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Acer
 */
public class SeparatorExample {

    JLabel l1, l2;

    public SeparatorExample() {
        JFrame f = new JFrame("Separator Example");
        f.setLayout(new GridLayout(0,1));
        l1 = new JLabel("Above Separator");
        JSeparator sep = new JSeparator();
        l2 = new JLabel("Below Separator");
        f.add(l1);f.add(sep);f.add(l2);
        f.setSize(400, 400);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new SeparatorExample();
    }
}
