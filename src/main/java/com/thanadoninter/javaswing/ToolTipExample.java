/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import javax.swing.*;
/**
 *
 * @author Acer
 */
public class ToolTipExample {
    public static void main(String[] args) {
        JFrame f=new JFrame("Password Field Example");
        
        JPasswordField value = new JPasswordField();
        value.setBounds(100, 100, 100, 30);
        value.setToolTipText("Enter your password.");
        JLabel l = new JLabel("Password");
        l.setBounds(20, 100, 80, 30);
        
        f.add(l);f.add(value);
        f.setSize(300,300);
        f.setLayout(null);
        f.setVisible(true);
    }
}
