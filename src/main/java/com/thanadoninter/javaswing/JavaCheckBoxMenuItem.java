/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author Acer
 */
public class JavaCheckBoxMenuItem {

    public JavaCheckBoxMenuItem() {
        JFrame f = new JFrame("JMenu Example");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JMenuBar mb = new JMenuBar();
        JMenu menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        mb.add(menu);
        JMenuItem mn1 = new JMenuItem("Open", KeyEvent.VK_N);
        menu.add(mn1);

        JCheckBoxMenuItem caseMenuItem = new JCheckBoxMenuItem("Option_1");
        caseMenuItem.setMnemonic(KeyEvent.VK_C);
        menu.add(caseMenuItem);

        ActionListener aListener = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                AbstractButton aButton = (AbstractButton) event.getSource();
                boolean selected = aButton.getModel().isSelected();
                String newLabel;
                Icon newIcon;
                if (selected) {
                    newLabel = "Value-1";
                } else {
                    newLabel = "Value-2";
                }
                aButton.setText(newLabel);
            }
        };

        caseMenuItem.addActionListener(aListener);
        f.setJMenuBar(mb);
        f.setSize(350, 250);
        f.setVisible(true);
    }
    public static void main(String[] args) {
        new JavaCheckBoxMenuItem();
    }

}
