/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;

/**
 *
 * @author Acer
 */
public class ScrollBarExample {

    public ScrollBarExample() {
        JFrame f = new JFrame("Scrollbar Example");
        final JLabel l = new JLabel();
        l.setHorizontalAlignment(JLabel.CENTER);
        l.setSize(400, 100);
        final JScrollBar s = new JScrollBar();
        s.setBounds(100, 100, 50, 100);
        f.add(s);f.add(l);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
        s.addAdjustmentListener(new AdjustmentListener() {
            public void adjustmentValueChanged(AdjustmentEvent e) {
                l.setText("Vertical Scrollbar value is:" + s.getValue());
            }
        });
    }

    public static void main(String[] args) {
        new ScrollBarExample();
    }

}
