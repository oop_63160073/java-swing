/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.javaswing;

import javax.swing.*;

/**
 *
 * @author Acer
 */
public class SliderExample extends JFrame {

    public SliderExample() {
        JSlider slider = new JSlider(JSlider.VERTICAL, 0, 200, 0);
        slider.setMinorTickSpacing(10);
        slider.setMajorTickSpacing(20);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        
        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String[] args) {
        SliderExample frame = new SliderExample();
        frame.pack();
        frame.setVisible(true);
    }
}
